# Load libraries required by the Evernote OAuth sample applications
require 'oauth'
require 'oauth/consumer'

# Add the Thrift & Evernote Ruby libraries to the load path.
# This will only work if you run this application from the /ruby/sample/oauth
# directory of the Evernote API SDK.
dir = File.expand_path(File.dirname(__FILE__))
$LOAD_PATH.push("#{dir}/lib")
$LOAD_PATH.push("#{dir}/lib/Evernote/EDAM")

require "thrift/types"
require "thrift/struct"
require "thrift/protocol/base_protocol"
require "thrift/protocol/binary_protocol"
require "thrift/transport/base_transport"
require "thrift/transport/http_client_transport"
require "Evernote/EDAM/note_store"

# Evernote Client credentials
# Fill these in with the consumer key and consumer secret that you obtained
# from Evernote. If you do not have an Evernote API key, you may request one
# from http://dev.evernote.com/documentation/cloud/
OAUTH_CONSUMER_KEY = "pyro2927-9032"
OAUTH_CONSUMER_SECRET = "0146d66d011ca4f7"

# Constants
# Replace this with https://www.evernote.com to use the Evernote production service
EVERNOTE_SERVER = "https://www.evernote.com"
REQUEST_TOKEN_URL = "#{EVERNOTE_SERVER}/oauth"
ACCESS_TOKEN_URL = "#{EVERNOTE_SERVER}/oauth"
AUTHORIZATION_URL = "#{EVERNOTE_SERVER}/OAuth.action"
NOTESTORE_URL_BASE = "#{EVERNOTE_SERVER}/edam/note/"

# Box Client creds
BOX_API_KEY = "tvxk2j65uchod6j5li1fis9m8vrkqe07"

# Box Server Constants
BOX_SERVER = "https://www.box.com"
BOX_TICKET_API_URL = "#{BOX_SERVER}/api/1.0/rest?action=get_ticket&api_key=#{BOX_API_KEY}"
BOX_USER_AUTH_URL = "#{BOX_SERVER}/api/1.0/auth/"
BOX_FOLDER_INFO_URL = "#{BOX_SERVER}/api/2.0/folders/"#FOLDER_ID
BOX_FILE_UPLOAD_URL = "#{BOX_SERVER}/api/2.0/files/data"

# configure PDFKit to use a local copy of wkhtmltopdf
PDFKit.configure do |config|
  # make sure to use the amd64 binary when in production on heroku
  config.wkhtmltopdf = (ENV['RACK_ENV'] == "production" ? "#{dir}/bin/wkhtmltopdf-amd64" : "#{dir}/bin/wkhtmltopdf" )
end