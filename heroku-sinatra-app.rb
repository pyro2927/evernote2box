# You'll need to require these if you
# want to develop while running with ruby.
# The config/rackup.ru requires these as well
# for it's own reasons.
#
# $ ruby heroku-sinatra-app.rb
#
require 'rubygems'
require 'sinatra'
require 'rest-client'
require 'dm-core'
require 'dm-migrations'
require 'pdfkit'
require 'box-api'
require 'digest/md5'
require 'json'

enable :sessions

# Load our dependencies and configuration settings
$LOAD_PATH.push(File.expand_path(File.dirname(__FILE__)))
require "config.rb"

# setup for use with foreman
configure do
    set :app_file, __FILE__
    set :port, ENV['PORT']
    set :static, true
    set :public_folder, '.'
end
  
before do
  if (session[:userId].nil? == false)
    @user = User.first(:evernote_id, session[:userId])
  end
end

# create our user object
class User
  include DataMapper::Resource
  property :id,               Serial
  property :evernote_id,      String
  property :box_id,           String
  property :box_auth,         String, :length => 100
  property :evernote_auth,    String, :length => 100
  property :note_store_url,   String, :length => 100
end


configure :development do
  DataMapper::Logger.new($stdout, :debug)
  DataMapper.setup(:default, "postgres://jykrmuiosclgau:IOKDaHUZjAWoFK6JwBiuJfLdRI@ec2-107-22-169-69.compute-1.amazonaws.com:5432/d62sfn8bni9ned")  #"sqlite3://#{Dir.pwd}/e2b.db")
  # make sure to migrate our database
  # DataMapper.auto_migrate!
	# finalize our data models
	DataMapper.finalize
end

configure :production do
  # Configure stuff here you'll want to
  # only be run at Heroku at boot

  # TIP:  You can get you database information
  #       from ENV['DATABASE_URI'] (see /env route below)
  # setup our data store
  DataMapper::Logger.new($stdout, :debug)
  unless ENV['HEROKU_POSTGRESQL_TEAL_URL'].nil?
  	DataMapper.setup(:default, ENV['HEROKU_POSTGRESQL_TEAL_URL'])
  else
    DataMapper.setup(:default, "sqlite3://#{Dir.pwd}/e2b.db")
  end
  # make sure to migrate our database
	DataMapper.auto_upgrade!
	# finalize our data models
	DataMapper.finalize
end

##
# Reset the session
##
get '/reset' do
  session[:request_token] = nil
  session[:oauth_verifier] = nil
  session[:userId] = nil
  @current_status = "Logged out succesfully"
  erb :index
end

##
# Index page
##
get '/' do
  erb :index
end

##
# Step 1: obtain token, then present user with oauth login screen
##
get '/requesttoken' do
  callback_url = request.url.chomp("requesttoken").concat("callback")

  begin
    consumer = OAuth::Consumer.new(OAUTH_CONSUMER_KEY, OAUTH_CONSUMER_SECRET,{
        :site => EVERNOTE_SERVER,
        :request_token_path => "/oauth",
        :access_token_path => "/oauth",
        :authorize_path => "/OAuth.action"})
    session[:request_token] = consumer.get_request_token(:oauth_callback => callback_url)
    redirect session[:request_token].authorize_url
  rescue Exception => e
    @last_error = "Error obtaining temporary credentials: #{e.message}"
    erb :error
  end
end

##
# Step 2b: receive callback from the Evernote authorization page
##
get '/callback' do
  if (params['oauth_verifier'].nil?)
    @last_error = "Content owner did not authorize the temporary credentials"
  else
    session[:oauth_verifier] = params['oauth_verifier']
    oauth_verifier = params['oauth_verifier']
    @current_status = "Content owner authorized the temporary credentials"
    # You shouldn't be invoking this if you don't have a request token
    if (session[:request_token].nil? == false)
      begin
        access_token = session[:request_token].get_access_token(:oauth_verifier => oauth_verifier)
        session[:access_token] = access_token
        session[:userId] = access_token.params['edam_userId']
        session[:noteStoreUrl] = session[:access_token].params['edam_noteStoreUrl']
        
        # create our current message
        @current_status = "Succesfully logged in"
      rescue Exception => e
        @last_error = "Failed to obtain token credentials from Evernote"      
      end
    end
    if (session[:userId].nil? == false)
      begin
        @user = User.first(:evernote_id, session[:userId])
        if (@user.nil?)
          @user = User.new
        end
        @user.evernote_id = session[:userId]
        @user.evernote_auth = session[:access_token].token
        @user.note_store_url = session[:noteStoreUrl]
        @user.save!
      rescue Exception => e
        @last_error = "Failed to save user account"
      end
    end
  end
  erb :index
end


##
# Box step 1: get a ticket
##

get '/requestboxtoken' do
  ticket_html = RestClient.get(BOX_TICKET_API_URL)
  # eww, XML, just get our ticket out w/ regex
  match_data = /<ticket>(.*)<\/ticket>/.match(ticket_html)
  # make sure we have a valid ticket
  unless match_data[1].nil?
    ticket = match_data[1]
    redirect BOX_USER_AUTH_URL + ticket
  else
    erb :index
  end
  
end

##
# Box step 2: get our callback
##
# sample url callback: /boxcallback?ticket=dnnq04b11btz48j0vhmu1drjhgakpk35&auth_token=dp39upr7a5vj4d4xt5kreqf0tz1ehp7u
get '/boxcallback' do
  if params.nil? or params[:auth_token].nil?
    @last_error = "Error receiving Authorization from Box"
  else
    # save our Box auth token in our database
    @user = User.first(:evernote_id, session[:userId])
    session[:box_auth_token] = params[:auth_token]
    @user.box_auth = params[:auth_token]
    @user.save
    
    # test our auth token
    # auth header skeleton: "Authorization: BoxAuth api_key=API_KEY&auth_token=AUTH_TOKEN"
    # root_directory_info = RestClient.get BOX_FOLDER_INFO_URL + "0", :authorization => "BoxAuth api_key=#{BOX_API_KEY}&auth_token=#{@user.box_auth}"
    @current_status = "Box account linked succesfully!"
  end
  
  erb :index
end

get '/test' do
  redirect "http://#{request.host_with_port}/boxnotes?userId=180191&guid=f8aa90ff-365e-40f0-8ef3-cc3ca7581d69&reason=create"
end

##
# Autonomous step: called when we update a note on Evernote
##
# url skeleton: http[s]://[your base URL][? |&]userId=[evernoteUserId]&guid=[noteGuid]&reason=[create | update]
# more info found on Evernote webhooks: http://dev.evernote.com/documentation/cloud/chapters/Polling_Notification.php#webhooks

get '/boxnotes' do
  @user = User.first(:evernote_id, params[:userId])
  # make sure we have a valid user to perform this transaction for
  unless @user.nil? || @user.evernote_auth.nil?
    # take this user's auth, and fetch our note
    noteStoreTransport = Thrift::HTTPClientTransport.new(@user.note_store_url)
    noteStoreProtocol = Thrift::BinaryProtocol.new(noteStoreTransport)
    noteStore = Evernote::EDAM::NoteStore::NoteStore::Client.new(noteStoreProtocol)
    note = noteStore.getNote(@user.evernote_auth, params[:guid], true, true, true, true)
    
    # make sure we have the correct tag, otherwise just ignore it
    has_tag = false
    tags = noteStore.getNoteTagNames(@user.evernote_auth, note.guid)
    unless tags.nil?
      tags.each do |tag|
        if tag == "BoxNotes"
          has_tag = true
        end
      end
    end
    
    if has_tag == true
      # make sure Heroku has a directory path that we can write to
      directory_path = Dir.pwd + "/tmp"
      unless Dir::exists?(directory_path)
        Dir::mkdir(directory_path)
      end
    
      note_html = note.content
      note_html.gsub!("en-media", "img")
      note_html.gsub!("hash=\"", "src=\"http://#{request.host_with_port}/tmp/")
      # loop through our resources and get them into our 
      unless note.resources.nil?
        note.resources.each do |resource|
          File.open(directory_path + "/" + Digest::MD5.hexdigest(resource.data.body), 'w') { |f| f.write(resource.data.body) }
        end
      end
    
      # generate our PDF with PDFKit
      kit = PDFKit.new("#{note.content}", :page_size => 'Letter')
      pdf = kit.to_pdf
      file_name = "#{note.title}.pdf"
      file_path = "#{directory_path}/#{file_name}"
      kit.to_file(file_path)

      # check to see if we have an existing file_id, or if we should be uploading a new file
      existing_file_id = ""
      url = ""
      begin
        existing_file_id = noteStore.getNoteApplicationDataEntry(@user.evernote_auth, note.guid, "file_id")
        url = "https://www.box.com/api/2.0/files/#{existing_file_id}/data/"
      rescue
        # new file, nothing really to rescue
        url = "https://www.box.com/api/2.0/files/data/"
      end


      # push our file up to box
      json_response = RestClient::Request.new(:method => :post,:url => url,:headers => {:authorization => "BoxAuth api_key=#{BOX_API_KEY}&auth_token=#{@user.box_auth}", :content_type => "multipart/form-data"}, :filename => file_name,:folder_id => "0", :payload => { :multipart => true, :file => File.new(file_path), :folder_id => "0"}).execute
      # check our file_id
      json = JSON.parse(json_response)
      box_file_id = json["entries"][0]["id"]
      # save our file_id back to our evernote
      unless box_file_id.nil?
        noteStore.setNoteApplicationDataEntry(@user.evernote_auth, note.guid, "file_id", box_file_id)
      end

      @current_status = "#{file_path} + #{json_response}"
    else
      @current_status = "Doesn't have required tag"
    end
  else
    @last_error = "User not found for this evernote ID"
  end
  erb :index
end

# delete page, allowing users to purge their credentials
get '/delete' do
  erb :delete
end

get '/deleteforreal' do
  @user = User.first(:evernote_id, params[:userId])
  @user.destroy
  @current_status = "User account deleted"
  session[:request_token] = nil
  session[:oauth_verifier] = nil
  session[:userId] = nil
  erb :index
end


# Test at <appname>.heroku.com

# You can see all your app specific information this way.
# IMPORTANT! This is a very bad thing to do for a production
# application with sensitive information

# get '/env' do
  # ENV.inspect
# end

__END__

@@ index
<html>
  <head>
    <title>Evernote2Box</title>
    <link href="./bootstrap/docs/assets/css/bootstrap.css" rel="stylesheet">
    <link href="./bootstrap/docs/assets/css/bootstrap-responsive.css" rel="stylesheet">
    <style>
          body {
            padding-top: 60px; /* 60px to make the container go all the way to the bottom of the topbar */
          }
        </style>
  </head>
  <body>
    <div class="navbar navbar-fixed-top"> <div class="navbar-inner"> <div class="container"> <a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse"> <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span> </a> <a class="brand" href="/">Evernote2Box</a> <div class="nav-collapse"> </div><!--/.nav-collapse --> </div> </div> </div>
    <div class="container">
      <!-- Main hero unit for a primary marketing message or call to action -->
      <div class="hero-unit">
        <% if (session[:userId].nil? == true || @user.nil?) %>
          <h1>Welcome!</h1>
          <p>Welcome to Evernote2Box, your one stop shop for Evernote to PDF-into-Box conversion!  To login with your Evernote account, please click the button below.</p>
          <p><a href="/requesttoken" class="btn btn-primary btn-large">Login &raquo;</a></p>
        <% elsif (@user.nil? == false and @user.box_auth.nil? == false) %>
          <h1>You're Done!</h1>
          <p>And that's it!  You have already linked both your Evernote and Box accounts, so any time you tag an Evernote with 'BoxNotes', it will automatically be converted into a PDF and placed into your Box!</p>
          <p>If you would like to delete your account, or link a different Box account, please select those buttons below.</p>
          <p><a href="/requestboxtoken" class="btn btn-info btn-large">Relink Box Account &raquo;</a>&nbsp;<a href="/reset" class="btn btn-primary btn-large">Logout &raquo;</a>&nbsp;<a href="/delete" class="btn btn-danger btn-large">Delete my account &raquo;</a></p>
        <% else %>
          <h1>Almost there...</h1>
          <p>You've linked your Evernote account, now all you need to do is link your Box account and you're all done!</p>
          <p><a href="/requestboxtoken" class="btn btn-info btn-large">Link Box Account &raquo;</p></a>
        <% end %>
      </div>
      <hr/>
      
      <p>
        <% if (@last_error.nil? == false) %>
          <div class="alert alert-error"><%= @last_error %></div>
        <% elsif (@current_status.nil? == false) %>
          <div class="alert alert-success"><%= @current_status %></div>
        <% end %>
      </p>
    </div>
  </body>
</html>

@@ delete
<html>
  <head>
    <title>Evernote2Box</title>
    <link href="./bootstrap/docs/assets/css/bootstrap.css" rel="stylesheet">
    <link href="./bootstrap/docs/assets/css/bootstrap-responsive.css" rel="stylesheet">
    <style>
          body {
            padding-top: 60px; /* 60px to make the container go all the way to the bottom of the topbar */
          }
        </style>
  </head>
  <body>
    <div class="navbar navbar-fixed-top"> <div class="navbar-inner"> <div class="container"> <a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse"> <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span> </a> <a class="brand" href="/">Evernote2Box</a> <div class="nav-collapse"> </div><!--/.nav-collapse --> </div> </div> </div>
    <div class="container">
      <!-- Main hero unit for a primary marketing message or call to action -->
      <div class="hero-unit">
          <h1>Are you sure?</h1>
          <p>If you delete your account here, your Evernotes tagged with 'BoxNotes will no longer be converted into PDFs and uploaded to Box.  If you are sure you want to do this, click 'Yes' below.</p>
          <p><a href="/" class="btn btn-success btn-large">No &raquo;</a>&nbsp;<a href="/deleteforreal" class="btn btn-danger btn-large">Yes &raquo;</a></p>
      </div>
      <hr/>
      
      <p>
        <% if (@last_error.nil? == false) %>
          <div class="alert alert-error"><%= @last_error %></div>
        <% elsif (@current_status.nil? == false) %>
          <div class="alert alert-success"><%= @current_status %></div>
        <% end %>
      </p>
    </div>
  </body>
</html>
